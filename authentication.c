#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <crypt.h>
#include <shadow.h>
#include <pwd.h>
#include <stdlib.h>

//execute this commande to compile code :   gcc authentication.c -lcrypt


int value;
char username[16];
char password[16];
char temp_password[16];
const char* encrypted;
char salt[4];
int i;
FILE *fp;
char request_val[100];   				//value read from file username
char request_salt[100];					//value read from file salt 
char request_enpwd[100];				//value read from file encrypted password
	 
const char * fn_encrypt(const char*password_val, const char*salt_val) {  //function to encrypt password with salt
   return crypt (password_val, salt_val);	 
  }	 
  
	 
int main()
{
printf("Hello World.\n");

do{														//chose between 1 and 2 . loop if value 1 or 2 is not inserted
printf("#### Choose 1: Enter a credential ####\n");
printf("#### Choose 2: Login into system #####\n");
fflush(stdout);
scanf("%d",&value);	
	
if ((value ==1) || value ==2){
	break;
}else{
	printf("#### Please choose value again#### \n");fflush(stdout);		//enter value again
}
	
}while (1);

if (value==1){   //enter credential	in system						
	
	
    printf("Enter name: ");fflush(stdout);
    scanf("%s", username);										//enter name
	printf("Enter 8 digit Password: ");fflush(stdout);
    scanf("%s", password);										//enter password
	
	printf("Your username is : %s & your password is : %s\n",username,password);		//display name and password
	printf("Encrypting...\n");
	
		
	for (i=0;i<16;i++){
		temp_password[i]=password[i];							//assign password to a temp variable
	}
	
	srand(time(NULL));
	int r = rand() % 4056 + 1;
	sprintf(salt, "%d", r); 								//convert integer to string
	printf("Salt value: %s\n", salt);						// generate random number between 0-4056 for salt
	
	for (i=0;i<5;i++){										//encrypt password five time with salt, after first time use encrypted value with salt
	encrypted=fn_encrypt(temp_password,salt);			
	printf("Encrypted password %d time: %s \n",i+1,encrypted);
	strncpy(temp_password,encrypted,16);   //new					//copy the encrypted to temp password variable
	}
	
	printf("Saving...\n");
	fp = fopen("/root/password.txt", "a");				//save data in a file username:salt:encryptedpassworrd
	fprintf(fp,"%s\t%d\t%s\n",username,r,encrypted);
	fclose(fp);
	 
}
else{     //login into system 
	
	printf("Enter name: ");fflush(stdout);
    scanf("%s", username);									//enter username
	printf("Enter 8 digit Password: ");fflush(stdout);
    scanf("%s", password);								//enter password
	
	FILE *file = fopen("/root/password.txt", "r"); 			//open file for reading
	int count=0;
	while(!feof(file)) {									//loop in file check for the presence of the username
    fscanf(file,"%s %s %s ",request_val,request_salt,request_enpwd);		// read each line
	if (strstr(request_val,username)!= NULL){				//check username
	printf("Username is valid\n");
	printf("Salt value: %s & Encrypted value: %s\n",request_salt,request_enpwd);
	count=1;											//if username is present , count =1
	break;
	}
	}
	
	if(count!=1){
	printf("Username is invalid\n");						//if count not equal to 1 display message invalid
	}else{
	printf("Validating password now...\n");					//else username present , continue with validating password
	
	for (i=0;i<16;i++){
		temp_password[i]=password[i];						//assign password to a temp variable
	}
	
	for (i=0;i<5;i++){	
	encrypted=fn_encrypt(temp_password,request_salt);		//encrypt password 	
	//printf("Encrypted password %d time: %s \n",i+1,encrypted);
	strncpy(temp_password,encrypted,16);   //new
	}
	printf("Encrypted password %s \n",encrypted);
	if(strstr(encrypted,request_enpwd)!= NULL){
	printf("Password valid...\n");
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	printf("Login now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	}else{
		printf("Password invalid & login failed...\n");
	}	
	}
}
    return 0;
}



